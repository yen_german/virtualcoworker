﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CardGameWebApp.Model;

namespace CardGameWebApp.DataLayer
{
    public class DLCardGame
    {
        string constr = ConfigurationManager.ConnectionStrings["dbCardGame"].ConnectionString;

        public List<CardSuit> GetCardSuit()
        {
            List<CardSuit> cardsuit = new List<CardSuit>();

            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Id, Suit FROM CardSuit"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        DataTable dt = new DataTable();

                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {
                            CardSuit s = new CardSuit();
                            s.Id = int.Parse(row[0].ToString());
                            s.Suit = row[1].ToString();
                            cardsuit.Add(s);
                        }
                    }
                }
            }

            return cardsuit;
        }

        public List<CardValue> GetCardValue()
        {
            List<CardValue> cardvalue = new List<CardValue>();

            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Id, Card, Value FROM CardValue"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        DataTable dt = new DataTable();

                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {
                            CardValue v = new CardValue();
                            v.Id = int.Parse(row[0].ToString());
                            v.Card = row[1].ToString();
                            v.Value = int.Parse(row[2].ToString());
                            cardvalue.Add(v);
                        }
                    }
                }
            }

            return cardvalue;
        }

        public List<WinnerHistory> GetWinnerHistory()
        {
            List<WinnerHistory> winnerhistory = new List<WinnerHistory>();

            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT PlayerName as Player, Points, WonDateTime as Date FROM WinnerHistory ORDER BY Points DESC"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        DataTable dt = new DataTable();

                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);

                        foreach (DataRow row in dt.Rows)
                        {
                            WinnerHistory v = new WinnerHistory();
                            v.PlayerName = row[0].ToString();
                            v.Points = int.Parse(row[1].ToString());
                            v.WonDateTime = DateTime.Parse(row[2].ToString());
                            winnerhistory.Add(v);
                        }
                    }
                }
            }

            return winnerhistory;
        }

        public bool SaveWinner(WinnerHistory winner)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    string saveWinner = "INSERT INTO WinnerHistory VALUES (@PlayerName, @Points, @WonDateTime)";

                    using (SqlCommand querysaveWinner = new SqlCommand(saveWinner))
                    {
                        querysaveWinner.Connection = con;
                        querysaveWinner.Parameters.Add("@PlayerName", SqlDbType.VarChar, 100).Value = winner.PlayerName;
                        querysaveWinner.Parameters.Add("@Points", SqlDbType.Int, 100).Value = winner.Points;
                        querysaveWinner.Parameters.Add("@WonDateTime", SqlDbType.DateTime, 100).Value = winner.WonDateTime;
                        con.Open();
                        querysaveWinner.ExecuteNonQuery();
                        con.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string s = ex.Message.ToString();
                return false;
            }

            return true;
        }
    }
}