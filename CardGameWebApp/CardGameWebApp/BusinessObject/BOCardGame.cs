﻿using System;
using System.Collections.Generic;
using CardGameWebApp.DataLayer;
using CardGameWebApp.Model;

namespace CardGameWebApp.BusinessObject
{
    public class BOCardGame
    {
        DLCardGame cardgame = new DLCardGame();

        public List<CardSuit> GetCardSuit()
        {
            return cardgame.GetCardSuit();
        }

        public List<CardValue> GetCardValue()
        {
            return cardgame.GetCardValue();
        }

        public List<WinnerHistory> GetWinnerHistory()
        {
            return cardgame.GetWinnerHistory();
        }

        public bool SaveWinner(WinnerHistory winner)
        {
            return cardgame.SaveWinner(winner);
        }
    }
}