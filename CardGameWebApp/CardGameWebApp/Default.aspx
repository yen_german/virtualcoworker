﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CardGameWebApp._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron" style="background-image: url('../images/cardclub.jpg');">
        <h1>A very simple card game.</h1>
        <h3>Input number of players and click Start Game to start playing.</h3>
        <p class="lead">Maximum of 8 players allowed and minimum of 2.</p>
        <p>
            <asp:TextBox ID="txtNumberOfPlayers" runat="server" Style="text-align: center;"></asp:TextBox>
        </p>
        <p>
            <asp:Button runat="server" ID="btnStartGame" Text="Start Game" class="btn btn-primary btn-lg" OnClick="btnStartGame_Click" />
        </p>
        <p>
            <asp:Label ID="lblMessage" runat="server" Visible="false" Style="color: red; font-size: 15px;"></asp:Label>
        </p>
    </div>

    <div class="row" style="text-align: center;">
        <div class="col-md-9" id="divGame" style="display:none;" runat="server">
            <h2>Wildcard</h2>
            <p>
                <asp:Label ID="lblWildCard" runat="server" class="btn btn-default"></asp:Label>
            </p>

            <asp:GridView ID="grdvwPlayers" runat="server" CssClass="table">
                <HeaderStyle BackColor="#EA6153" ForeColor="White" />
                <RowStyle BorderStyle="None" BorderWidth="0px" />
            </asp:GridView>

            <h2>Winner</h2>
            <p>
                <asp:Label ID="lblWinner" runat="server" class="btn btn-default"></asp:Label>
            </p>
        </div>
        <div class="col-md-3" id="divLeaderBoard" runat="server" style="display:none;" >
            <h2>Leaderboard</h2>
            <asp:GridView ID="grdvwLeaderBoard" runat="server" AllowPaging="True" OnPageIndexChanging="grdvwLeaderBoard_PageIndexChanging" CssClass="table">
                <HeaderStyle BackColor="#EA6153" ForeColor="White" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                <RowStyle BorderStyle="None" BorderWidth="0px" />
            </asp:GridView>
        </div>
    </div>

</asp:Content>
