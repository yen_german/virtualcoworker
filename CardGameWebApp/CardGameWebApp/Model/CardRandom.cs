﻿using System;
using System.Collections.Generic;

namespace CardGameWebApp.Model
{
    public class CardRandom
    {
        public int Id { get; set; }
        public string PlayerName { get; set; }
        public string FirstCard { get; set; }
        public string SecondCard { get; set; }
        public string ThirdCard { get; set; }
        public string FourthCard { get; set; }
        public string FifthCard { get; set; }
        public string TotalPoints { get; set; }
        public int TotalPointsNum { get; set; }
        public bool WinnerFl { get; set; }
    }
}