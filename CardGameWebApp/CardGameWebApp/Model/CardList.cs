﻿using System;
using System.Collections.Generic;

namespace CardGameWebApp.Model
{
    public class CardList
    {
        public string Card { get; set; }
        public int Value { get; set; }
        public string Suit { get; set; }
    }
}