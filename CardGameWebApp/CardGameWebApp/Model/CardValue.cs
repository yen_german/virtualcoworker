﻿using System;
using System.Collections.Generic;

namespace CardGameWebApp.Model
{
    public class CardValue
    {
        public int Id { get; set; }
        public string Card { get; set; }
        public int Value { get; set; }
    }
}