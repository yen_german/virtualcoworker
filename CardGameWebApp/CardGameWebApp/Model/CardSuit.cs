﻿using System;
using System.Collections.Generic;

namespace CardGameWebApp.Model
{
    public class CardSuit
    {
        public int Id { get; set; }
        public string Suit { get; set; }
    }
}