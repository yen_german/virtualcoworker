﻿using System;
using System.Collections.Generic;

namespace CardGameWebApp.Model
{
    public class WinnerHistory
    {
        public string PlayerName { get; set; }
        public int Points { get; set; }
        public DateTime WonDateTime { get; set; }
    }
}