﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CardGameWebApp.Model;
using CardGameWebApp.BusinessObject;
using System.Data;
using System.Reflection;
using System.ComponentModel;

namespace CardGameWebApp
{
    public partial class _Default : Page
    {
        BOCardGame boCardGame = new BOCardGame();
        List<CardList> listCard = new List<CardList>();
        DataTable dt = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            getCards();
            getLeaderBoard();
        }

        #region User-Defined Functions

        protected void getCards()
        {
            // Retrieve Cards

            List<CardSuit> cardsuit = new List<CardSuit>();
            cardsuit = boCardGame.GetCardSuit();

            List<CardValue> cardvalue = new List<CardValue>();
            cardvalue = boCardGame.GetCardValue();

            // Prepare the cards

            if (cardsuit.Count > 0 && cardsuit.Count > 0)
            {
                foreach (CardSuit suit in cardsuit)
                {
                    foreach (CardValue value in cardvalue)
                    {
                        CardList c = new CardList();
                        c.Suit = suit.Suit;
                        c.Card = value.Card;
                        c.Value = value.Value;
                        listCard.Add(c);
                    }
                }
            }
        }

        protected void getLeaderBoard()
        {
            grdvwLeaderBoard.DataSource = boCardGame.GetWinnerHistory();
            grdvwLeaderBoard.DataBind();

            if (grdvwLeaderBoard.Rows.Count > 0)
            {
                grdvwLeaderBoard.HeaderRow.Cells[0].Text = "Player";
                grdvwLeaderBoard.HeaderRow.Cells[2].Text = "Date";

                divLeaderBoard.Style.Add("display", "block");
                divLeaderBoard.Style.Add("float", "right");
            }
        }

        void startGame(int iPlayerCount)
        {
            string sMethod = "startGame";

            List<CardList> listCardRandom = new List<CardList>();
            List<CardRandom> listRandom = new List<CardRandom>();


            int iTempWildCard = 0;
            string tempWildCard = string.Empty;
            string strPlayerName = string.Empty;

            try
            {
                listCardRandom = listCard;

                // Randomize and display Wild Card. Remove card from listCard.

                Random rnd = new Random();
                int r = rnd.Next(listCardRandom.Count);

                CardList randWildCard = new CardList();
                randWildCard = listCardRandom[r];

                if (int.TryParse(randWildCard.Card, out iTempWildCard))
                {
                    if (iTempWildCard > 0)
                        tempWildCard = randWildCard.Card + " " + randWildCard.Suit;
                }

                if (iTempWildCard == 0)
                {
                    tempWildCard = randWildCard.Card + " of " + randWildCard.Suit;
                }

                lblWildCard.Text = tempWildCard;
                listCardRandom.Remove(randWildCard);

                // Add Player Rows

                for (int i = 0; i < iPlayerCount; i++)
                {
                    strPlayerName = "Player # " + (i + 1).ToString();

                    CardRandom card = new CardRandom();
                    card.Id = i;
                    card.PlayerName = strPlayerName;
                    listRandom.Add(card);
                }

                DataTable dt = listRandom.ToDataTable();

                // Randomize 5 cards for each player and remove all these cards from listCard

                foreach (DataRow row in dt.Rows)
                {
                    int totalPoints = 0;
                    for (int i = 0; i < dt.Columns.Count - 1; i++)
                    {
                        if (i == dt.Columns.Count - 3)
                        {
                            row[i] = totalPoints.ToString() + " points";
                        }
                        else if (i == dt.Columns.Count - 2)
                        {
                            row[i] = totalPoints;
                        }
                        else if (i > 1)
                        {
                            Random crdrnd = new Random();
                            int c = rnd.Next(listCardRandom.Count);

                            CardList randCard = new CardList();
                            randCard = listCardRandom[c];

                            row[i] = string.Format("{0} {1} ({2} points)", randCard.Card, randCard.Suit, randCard.Value);

                            listCardRandom.Remove(randCard);

                            totalPoints += randCard.Suit == randWildCard.Suit ? randCard.Value * 2 : randCard.Value;
                        }
                    }
                }

                // Show and save Winner

                WinnerHistory wh = new WinnerHistory();
                var maxRow = dt.Select("TotalPointsNum = MAX(TotalPointsNum)");
                for (int i = 0; i < maxRow.Length; i++)
                {
                    maxRow[i][9] = true;
                    lblWinner.Text = maxRow[i][1].ToString();

                    wh.PlayerName = lblWinner.Text;
                    wh.Points = int.Parse(maxRow[i][8].ToString());
                    wh.WonDateTime = DateTime.Now;

                    boCardGame.SaveWinner(wh);

                    break;
                }

                dt.Columns.Remove("Id");
                dt.Columns.Remove("TotalPointsNum");
                dt.Columns.Remove("WinnerFl");

                grdvwPlayers.DataSource = dt;
                grdvwPlayers.DataBind();

                divGame.Style.Add("display", "block");

                formatGrdVwHeaders();
            }
            catch (Exception ex)
            {
                showExceptionMessage(sMethod, ex.Message.ToString());
            }
        }

        protected void formatGrdVwHeaders()
        {
            grdvwPlayers.HeaderRow.Cells[0].Text = "Player";
            grdvwPlayers.HeaderRow.Cells[1].Text = string.Empty;
            grdvwPlayers.HeaderRow.Cells[2].Text = string.Empty;
            grdvwPlayers.HeaderRow.Cells[3].Text = string.Empty;
            grdvwPlayers.HeaderRow.Cells[4].Text = string.Empty;
            grdvwPlayers.HeaderRow.Cells[5].Text = string.Empty;
            grdvwPlayers.HeaderRow.Cells[6].Text = "Points";
        }

        protected void showExceptionMessage(string sMethod, string sExMessage)
        {
            lblMessage.Text = string.Format("{0} [Call Stack Method: {1}]", sExMessage, sMethod);
            lblMessage.Visible = true;
        }

        #endregion

        #region Events

        protected void btnStartGame_Click(object sender, EventArgs e)
        {
            string sMethod = "btnStartGame_Click";
            int iPlayerCount = 0;

            try
            {
                lblMessage.Visible = false;

                if (!string.IsNullOrEmpty(txtNumberOfPlayers.Text))
                {
                    int.TryParse(txtNumberOfPlayers.Text, out iPlayerCount);
                    if (iPlayerCount > 1 && iPlayerCount < 9)
                    {
                        startGame(iPlayerCount);
                    }
                    else
                    {
                        showExceptionMessage(sMethod, "Please input valid number of players.");
                        divGame.Style.Add("display", "none");
                    }
                }
                else
                {
                    showExceptionMessage(sMethod, "Please input valid number of players.");
                    divGame.Style.Add("display", "none");
                }
            }
            catch (Exception ex)
            {
                showExceptionMessage(sMethod, ex.Message.ToString());
                divGame.Style.Add("display", "none");
            }
        }
       
        protected void grdvwLeaderBoard_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdvwLeaderBoard.PageIndex = e.NewPageIndex;
            getLeaderBoard();
        }

        #endregion
    }

    public static class Extensions
    {
        public static DataTable ToDataTable<T>(this List<T> iList)
        {
            DataTable dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptorCollection =
                TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
    }
}